// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var cookieParser = require('cookie-parser');
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// functions
var generate_token = (function(login, mdp){
	var CryptoJS = require("crypto-js");
	var token = CryptoJS.HmacSHA3(login + mdp, new Date().getTime().toString());
	token = token.toString(CryptoJS.enc.Base6);

	return(token);
});

//Template engine, pug
app.set('views', './views');
app.set('view engine', 'pug');
// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.use(cookieParser());

//Data view
app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

//Procedure
app.get("/", function(req, res, next){
	//If identification's error
	if(req.query.error == 1){
		res.render("connexion", {
			error : true,
			message : "Login ou mot de passe incorrect.",
		});
	}

	//If token cookie exists...
	if(req.cookies['token']){

		db.all("SELECT * FROM sessions WHERE token = ?", [req.cookies['token']], function(err, data){
			//Error
			if(err){
				throw err;
			}
			//If token isn't in sessions table, go to connexion page
			if(data.length == 0){
				res.render('connexion', {});
			}
			//Else if token is in sessions table go to index page for members with login
			else{
				var login;
				data.forEach(function(value){
				login = value.ident;
				});
				res.render('index', {
					pseudo : login,
				});
			}

		});

	}
	//Else if token cookie doesn't exist, go to connexion page
	else{
		res.render('connexion', {});
	}

});

app.get("/login", function(req, res, next){
	res.redirect("/");
});

app.post("/login", function(req, res, next) {

	var login = req.body.login;
	var mdp = req.body.mdp;

	var requete = "SELECT * FROM users WHERE ident = ? AND password = ?";

	db.all(requete, [login, mdp], function(err, data) {
		//Error
		if(err){
			throw err;
		}
		//If member isn't in users table, go to index page
		if(data.length == 0){
			res.redirect('/?error=1');
		}
		else{
			var requeteCheckAct = "SELECT * FROM sessions WHERE ident = ?";
			var tmpToken;
			tmpToken = generate_token(login, mdp);

			db.all(requeteCheckAct, [login], function(err, data) {
				//Error
				if(err){
					throw error;
				}

				var requete2;

				//Create or update token and insert in sessions table
				if(data.length == 0){
					requete2 = "INSERT INTO sessions VALUES (?, ?)";
					db.run(requete2, [login, tmpToken], function(data){
					});
				}
				else{
					requete2 = "UPDATE sessions SET token = ? WHERE ident = ?";
					db.run(requete2, [tmpToken, login], function(err, data){
					});
				}
			});


			res.cookie('token', tmpToken);
			res.redirect("/");
		}
	});
});

app.get("/deconnexion", function(req, res, next){
	res.clearCookie("token");
	res.redirect("/");
});


// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");


});
